class ChangeColumnStatusToRole < ActiveRecord::Migration
  def change
    remove_column :roles, :status
    add_column :roles, :active, :boolean
  end
end
