class Role < ActiveRecord::Base
  scope :active,->{where(active: true)}
  has_many :users_roles
  has_many :users,-> { distinct }, :through => :users_roles
end
