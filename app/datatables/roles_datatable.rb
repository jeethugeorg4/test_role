class RolesDatatable
  delegate :params, :h, :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: Role.count,
        iTotalDisplayRecords: roles.total_entries,
        aaData: data
    }
  end

  private

  def data

    roles.map do |role|
      [
          link_to(role.title, role),
          link_to(role.active? ? "Disable" : "Enable", @view.toggle_status_role_path(role),id: "role-toggle-#{role.id}", class: 'btn btn-primary',method: :post,remote: true)

      ]
    end
  end

  def roles
    @roles ||= fetch_roles
  end

  def fetch_roles
    roles = Role.order("#{sort_column} #{sort_direction}")
    roles = roles.page(page).per_page(per_page)
    if params[:sSearch].present?
      roles = roles.where("title like :search", search: "%#{params[:sSearch]}%")
    end
    roles
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[title active]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end

